package contact;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.util.HashMap;
import java.util.Map;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class ContactRestAssuredTest {
  @Test
  @DisplayName("Test STATUS OK API Contact")
  public void verifyStatusOkContact() {
    RestAssured.baseURI = "http://localhost:8080";
    RequestSpecification httpRequest = RestAssured.given();
    Response response = httpRequest.get("/contact");
    int statusCode = response.getStatusCode();
    Assertions.assertEquals(statusCode, 200, "Correct status");
  }

  @Test
  @DisplayName("Test POST API Contact")
  public void verifyPostApiContact() {
    RestAssured.baseURI = "http://localhost:8080";
    RequestSpecification httpRequest = RestAssured.given();
    Map<String, String> contact = new HashMap<>();
    contact.put("name", "aaa");
    contact.put("email", "bbb.ccc@tedregal.com");
    contact.put("contactNumber", "987654321");
    contact.put("description", "ddd");
    httpRequest.contentType("application/json").body(contact)
      .when().post("/contact").then().statusCode(200);
  }

  @Test
  @DisplayName("Test GET API Contact")
  public void verifyGetApiContact() {
    RestAssured.baseURI = "http://localhost:8080";
    RequestSpecification httpRequest = RestAssured.given();
    httpRequest.when().get("/contact").then()
      .body("name[0]", CoreMatchers.equalTo("aaa"))
      .body("email[0]", CoreMatchers.equalTo("bbb.ccc@tedregal.com"))
      .body("contactNumber[0]", CoreMatchers.equalTo("987654321"))
      .body("description[0]", CoreMatchers.equalTo("ddd"))
      .statusCode(200);
  }
}
